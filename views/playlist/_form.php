<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div id='modal-content'>
    <?php
    $form = ActiveForm::begin([
        'id' => $action . '-form' . ((empty($model->id)) ? '' : '-' . $model->id),
        'action' => (empty($model->id)) ? ['/playlist/' . $action] : ['/playlist/' . $action, 'id' => $model->id],
    ]);
    ?>

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'url') ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>