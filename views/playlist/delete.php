<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Playlist';
?>

<?php
$form = ActiveForm::begin([
    'id' => 'quick-form',
    'action' => ['/playlist/delete', 'id' => $model->id],
]);
?>
Вы действительно хотите удалить это видео?
</br>
</br>
<div class="form-group">
    <?= Html::submitButton('Да', ['class' => 'btn btn-primary']) ?>
    <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
</div>

<?php ActiveForm::end(); ?>
