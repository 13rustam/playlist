<?php

/* @var $this yii\web\View */

use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = 'Playlist';
?>
<?php if (!empty($successMessages)) { ?>

    <div class="alert alert-success" role="alert">
        <span class="glyphicon glyphicon-ok"></span> <?= Html::encode($successMessages); ?>
    </div>

<?php } else if (!empty($errorMessages)) { ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-warning-sign"></span> <?= Html::encode($errorMessages); ?>
    </div>
<?php } ?>
<section class="content">
    <ul>
        <?php foreach ($playlist as $item) { ?>
            <li>
                <h3 class="al-title"><?= $item->title ?></h3>
                <?php
                Modal::begin([
                    'toggleButton' => [
                        'label' => '<i class="glyphicon glyphicon-play"></i> Посмотреть',
                    ],
                    'closeButton' => [
                        'label' => 'Close',
                        'class' => 'btn btn-danger btn-sm pull-right',
                    ],
                    'size' => 'modal-lg',
                ]);
                echo $this->render('/playlist/view', ['model' => $item]);
                Modal::end();
                ?>
                <div><?= date('Y-m-d H:i:s', $item->created_at) ?></div>
                <p class="al-descr"><a href="<?= $item->url ?>" target="_blank"><?= $item->url ?></a></p>
                <?php
                Modal::begin([
                    'toggleButton' => [
                        'label' => '<i class="glyphicon glyphicon-edit"></i> Редактировать',
                        'class' => 'btn btn-warning',
                        'id' => 'modal-btn-edit-' . $item->id,
                    ],
                    'closeButton' => [
                        'label' => 'Close',
                        'class' => 'btn btn-danger btn-sm pull-right',
                    ],
                    'size' => 'modal-lg',
                ]);
                echo $this->render('/playlist/_form', ['model' => $item, 'action' => 'edit']);
                Modal::end();
                ?>
                <?php
                Modal::begin([
                    'toggleButton' => [
                        'label' => '<i class="glyphicon glyphicon-remove"></i> Удалить',
                        'class' => 'btn btn-danger',
                        'id' => 'modal-btn-delete-' . $item->id,
                    ],
                    'closeButton' => [
                        'label' => 'Close',
                        'class' => 'btn btn-danger btn-sm pull-right',
                    ],
                    'size' => 'modal-lg',
                ]);
                echo $this->render('/playlist/delete', ['model' => $item]);
                Modal::end();
                ?>
            </li>
        <?php } ?>
    </ul>
</section>

<?php
Modal::begin([
    'toggleButton' => [
        'label' => '<i class="glyphicon glyphicon-plus"></i> Добавить',
        'class' => 'btn btn-success',
        'id' => 'modal-btn-add',
    ],
    'closeButton' => [
        'label' => 'Close',
        'class' => 'btn btn-danger btn-sm pull-right',
    ],
    'size' => 'modal-lg',
]);
echo $this->render('/playlist/_form', ['model' => $model, 'action' => 'add']);
Modal::end();
?>
