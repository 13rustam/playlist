<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div id='modal-content'>
    <?php
    $form = ActiveForm::begin([
        'id' => 'form' . ((empty($model->id)) ? '' : '-' . $model->id),
    ]);
    ?>
    <?= preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe width=\"640\" height=\"360\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
        $model->url
    ) ?>

    <?php ActiveForm::end(); ?>

</div>