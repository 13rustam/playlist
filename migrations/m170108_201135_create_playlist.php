<?php

use yii\db\Migration;

class m170108_201135_create_playlist extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('playlist', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->defaultValue(''),
            'url' => $this->string()->notNull()->defaultValue(''),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'order' => $this->integer()->notNull()->defaultValue(0),
            'provider' => $this->string()->notNull()->defaultValue('youtube'),
            'UNIQUE KEY `unq_url` (`url`)'
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('playlist');
    }
}
