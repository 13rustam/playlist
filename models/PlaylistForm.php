<?php

namespace app\models;

use yii\db\ActiveRecord;

class PlaylistForm extends ActiveRecord
{
    public static function tableName()
    {
        return 'playlist';
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            ['url', 'required'],
            ['url', 'validateUrl', 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'url' => 'Ссылка на ролик',
        ];
    }

    public function validateUrl($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $rx = '~
                ^(?:https?://)?
                 (?:www\.)?
                 (?:youtube\.com)
                 /watch\?v=([^&]+)
                 ~x';

            if (!preg_match($rx, $this->url, $matches)) {
                $this->addError($attribute, 'Неправильная ссылка на ролик');
            }
            $model = self::find()->where(['url' => $this->url])->one();
            if (!empty($model)) {
                $this->addError($attribute, 'Данный ролик существует');
            }
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $date = new \DateTime();
                $this->created_at = strtotime($date->format('Y-m-d h:i:s'));
            }

            return true;
        }

        return false;
    }
}