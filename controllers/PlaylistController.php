<?php

namespace app\controllers;

use app\models\PlaylistForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class PlaylistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'main2';
        $session = Yii::$app->session;

        $model = new PlaylistForm();
        $playlist = PlaylistForm::find()->orderBy('created_at')->all();

        $errorMessages = $session->getFlash('error');
        $successMessages = $session->getFlash('success');

        return $this->render('index', [
            'model' => $model,
            'playlist' => $playlist,
            'errorMessages' => $errorMessages,
            'successMessages' => $successMessages,
        ]);
    }

    public function actionAdd()
    {
        $this->layout = 'main2';
        $model = new PlaylistForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Добавлено видео');
            return $this->redirect('index');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при добавлении');
            return $this->render('_form', [
                'model' => $model,
                'action' => 'add',
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->layout = 'main2';
        $model = PlaylistForm::findOne(['id' => $id]);
        $session = Yii::$app->session;

        if ($model->delete()) {
            $session->setFlash('success', 'Запись удалена');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при удалении');
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionEdit($id)
    {
        $this->layout = 'main2';
        $model = PlaylistForm::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect('index');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при редактирование');
            return $this->render('_form', [
                'model' => $model,
                'action' => 'edit',
            ]);
        }
    }
}
